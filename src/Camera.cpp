
#include <vector>
#include <iostream>
#include "pgr.h"
#include "Camera.h"

Camera::Camera(glm::vec3 position, glm::vec3 up, GLfloat yaw, GLfloat pitch) : front(glm::vec3(0.0f, 0.0f, -1.0f)), movementSpeed(SPEED), mouseSensitivity(SENSITIVTY) {
    this->position = position;
    this->worldUp = up;
    this->yaw = yaw;
    this->pitch = pitch;
    this->flyingEnabled = true;
    this->updateCameraVectors();
}

/**
 * @return projection matrix 
 */
glm::mat4 Camera::getViewMatrix() {
    return glm::lookAt(this->position, this->position + this->front, this->up);
}

/**
 * moving aroud scene by keyboard
 * @param direction by Camera_Movement enum to abstract from specific keyboard
 * @param deltaTime
 */
void Camera::processKeyboard(Camera_Movement direction, GLfloat deltaTime) {
    GLfloat velocity = this->movementSpeed * deltaTime;
    if (direction == FORWARD) {
        this->position += this->front * velocity;
    }
    if (direction == BACKWARD) {
        this->position -= this->front * velocity;
    }
    if (direction == LEFT) {
        this->position -= this->right * velocity;
    }
    if (direction == RIGHT) {
        this->position += this->right * velocity;
    }
    if (!this->flyingEnabled) {
        this->position.y = 0.01f;
    } else if (this->position.y < 0.01f) {
        this->position.y = 0.01f;
    }
}

/**
 * looking around by mouse
 * @param xoffset
 * @param yoffset
 * @param constrainPitch to prevent world desctruction by fliping 
 */
void Camera::processMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch) {
    xoffset *= this->mouseSensitivity;
    yoffset *= this->mouseSensitivity;

    this->yaw += xoffset;
    this->pitch += yoffset;

    if (constrainPitch) {
        float maxAngle = 89.0f;
        if (this->pitch > maxAngle)
            this->pitch = maxAngle;
        if (this->pitch < -maxAngle)
            this->pitch = -maxAngle;
    }
    this->updateCameraVectors();
}

void Camera::reset() {
    this->position = glm::vec3(0.0f, 0.0f, 0.7f);
    this->worldUp = glm::vec3(0.0f, 1.0f, 0.0f);
    this->front = glm::vec3(0.0f, 0.0f, -1.0f);
    this->yaw = YAW;
    this->pitch = PITCH;
    this->updateCameraVectors();
}

void Camera::setView1() {
    this->position = glm::vec3(1.0, 0.01, -0.859909);
    this->yaw = -232.60;
    this->pitch = 6.80001;
    updateCameraVectors();
}

void Camera::setView2() {
    this->position = glm::vec3(-0.654507, 2.19758, -0.711511);
    this->yaw = 67.69;
    this->pitch = -64.80001;
    updateCameraVectors();
}

void Camera::setFlying(bool set) {
    this->flyingEnabled = set;
    std::cout << "flying changed" << std::endl;
}

bool Camera::isFlying() {
    return this->flyingEnabled;
}

GLfloat Camera::getMouseSensitivity() const {
    return mouseSensitivity;
}

void Camera::setMouseSensitivity(GLfloat mouseSensitivity) {
    this->mouseSensitivity = mouseSensitivity;
}

GLfloat Camera::getMovementSpeed() const {
    return movementSpeed;
}

void Camera::setMovementSpeed(GLfloat movementSpeed) {
    this->movementSpeed = movementSpeed;
}

glm::vec3 Camera::getPosition() const {
    return position;
}

glm::vec3 Camera::getFront() const {
    return front;
}

void Camera::setPosition(glm::vec3 position) {
    this->position = position;
    updateCameraVectors();
}

void Camera::setFront(glm::vec3 front){
    this->front = front;
    updateCameraVectors();
}

void Camera::updateCameraVectors() {
    // Calculate the new Front vector
    glm::vec3 front;
    front.x = glm::cos(glm::radians(this->yaw)) * glm::cos(glm::radians(this->pitch));
    front.y = glm::sin(glm::radians(this->pitch));
    front.z = glm::sin(glm::radians(this->yaw)) * glm::cos(glm::radians(this->pitch));
    this->front = glm::normalize(front);
    this->right = glm::normalize(glm::cross(this->front, this->worldUp));
    this->up = glm::normalize(glm::cross(this->right, this->front));

//    std::cout << "Position " << this->position.x << " " << this->position.y << " " << this->position.z << " " << std::endl;
//    std::cout << "Front    " << this->front.x << " " << this->front.y << " " << this->front.z << " " << std::endl;
//    std::cout << "Right    " << this->right.x << " " << this->right.y << " " << this->right.z << " " << std::endl;
//    std::cout << "Up       " << this->up.x << " " << this->up.y << " " << this->up.z << " " << std::endl;
//    std::cout << "Yaw      " << this->yaw << std::endl;
//    std::cout << "Pitch    " << this->pitch << std::endl;
//    std::cout << std::endl;

}
