#include <time.h>
#include <list>
#include <iostream>
#include <X11/Xlib.h>
#include "pgr.h"
#include "data.h"
#include "render.h"
#include "helpers.h"
#include "init.h"

extern SCommonShaderProgram shaderProgram;
extern SPointLightShaderProgram plShaderProgram;
extern SSbShd skyboxShader;

typedef std::vector<void *> GameObjectsList;

SceneState sceneState;
SceneObjects sceneObjects;

void cleanUpObjects(void) {

    // delete trees
    while (!sceneObjects.trees.empty()) {
        //delete sceneObjects.trees.back();
        sceneObjects.trees.pop_back();
    }

    //delete glowWorms
    while (!sceneObjects.glowWorms.empty()) {
        //delete sceneObjects.glowWorms.back();
        sceneObjects.glowWorms.pop_back();
    }
}

void restartScene(void) {
    cleanUpObjects();

    sceneState.elapsedTime = 0.001f * (float) glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds

    //init ground
    if (sceneObjects.ground == NULL) {
        sceneObjects.ground = createGround(sceneState);
        std::cout << "ground" << std::endl;
    }
    sceneObjects.ground->position = glm::vec3(0.f, -0.1f, 0.0f);

    //(re)init house
    if (sceneObjects.house == NULL) {
        sceneObjects.house = new HouseObject();
        std::cout << "house" << std::endl;
    }
    sceneObjects.house->size = HOUSE_SIZE;
    sceneObjects.house->position = HOUSE_POSITION;

    //(re)init cart
    if (sceneObjects.cart == NULL) {
        sceneObjects.cart = new CartObject();
        std::cout << "cart" << std::endl;
    }
    sceneObjects.cart->size = CART_SIZE;
    sceneObjects.cart->position = CART_POSITION;
    sceneObjects.cart->direction = glm::vec3(0.0f, 0.0f, 1.0f);


    // initialize tress
    for (int i = 0; i < TREES_COUNT_MIN; i++) {
        TreeObject* newTree = createTree(sceneState);
        sceneObjects.trees.push_back(newTree);
    }

    //reinit glowworms
    for (int i = 0; i < GLOW_WORM_COUNT; i++) {
        GlowWormObject* elem = createGlowWorm(sceneState, wormsColors[i]);
        sceneObjects.glowWorms.push_back(elem);
    }

    if (sceneObjects.cube == NULL) {
        sceneObjects.cube = new CubeObject();
    }
    sceneObjects.cube->position = CUBE_POSITION;
    sceneObjects.cube->size = CUBE_SIZE;

    if (sceneObjects.explosion == NULL) {
        sceneObjects.explosion = new ExplosionObject();
    }
    sceneObjects.explosion->position = BANNER_POSITION;
    sceneObjects.explosion->size = BANNER_SIZE;
    sceneObjects.explosion->frameDuration = 1.1f;

    if (sceneState.freeCameraMode == true) {
        sceneState.freeCameraMode = false;
        glutPassiveMotionFunc(NULL);
    }
    if (sceneObjects.camera == NULL) {
        sceneObjects.camera = new Camera;
    }
    sceneObjects.camera->reset();
    sceneObjects.camera->setFlying(false);

    for (int i = 0; i < KEYS_COUNT; i++)
        sceneState.keyMap[i] = false;
}

void setupLights(GLuint programId, glm::vec3 pointLightPositions[], glm::vec3 pointLightColors[]) {
    GLfloat linear = 0.7f;
    GLfloat quaratic = 1.8f;
    // Directional light
    glUniform3f(glGetUniformLocation(programId, "dirLight.direction"), -0.2f, -1.0f, -0.3f);
    glUniform3f(glGetUniformLocation(programId, "dirLight.ambient"), 0.05f, 0.05f, 0.05f);
    glUniform3f(glGetUniformLocation(programId, "dirLight.diffuse"), 0.2f, 0.2f, 0.2f);
    glUniform3f(glGetUniformLocation(programId, "dirLight.specular"), 0.3f, 0.3f, 0.3f);

    // Point light 1
    glUniform3f(glGetUniformLocation(programId, "pointLights[0].position"), pointLightPositions[0].x, pointLightPositions[0].y, pointLightPositions[0].z);
    glUniform3f(glGetUniformLocation(programId, "pointLights[0].ambient"), pointLightColors[0].x * 0.1, pointLightColors[0].y * 0.1, pointLightColors[0].z * 0.1);
    glUniform3f(glGetUniformLocation(programId, "pointLights[0].diffuse"), pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);
    glUniform3f(glGetUniformLocation(programId, "pointLights[0].specular"), pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);

    glUniform1f(glGetUniformLocation(programId, "pointLights[0].constant"), 1.0f);
    glUniform1f(glGetUniformLocation(programId, "pointLights[0].linear"), linear);
    glUniform1f(glGetUniformLocation(programId, "pointLights[0].quadratic"), quaratic);
    // Point light 2
    glUniform3f(glGetUniformLocation(programId, "pointLights[1].position"), pointLightPositions[1].x, pointLightPositions[1].y, pointLightPositions[1].z);
    glUniform3f(glGetUniformLocation(programId, "pointLights[1].ambient"), pointLightColors[0].x * 0.1, pointLightColors[0].y * 0.1, pointLightColors[0].z * 0.1);
    glUniform3f(glGetUniformLocation(programId, "pointLights[1].diffuse"), pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);
    glUniform3f(glGetUniformLocation(programId, "pointLights[1].specular"), pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);

    glUniform1f(glGetUniformLocation(programId, "pointLights[1].constant"), 1.0f);
    glUniform1f(glGetUniformLocation(programId, "pointLights[1].linear"), linear);
    glUniform1f(glGetUniformLocation(programId, "pointLights[1].quadratic"), quaratic);
    // Point light 3
    glUniform3f(glGetUniformLocation(programId, "pointLights[2].position"), pointLightPositions[2].x, pointLightPositions[2].y, pointLightPositions[2].z);
    glUniform3f(glGetUniformLocation(programId, "pointLights[2].ambient"), pointLightColors[0].x * 0.1, pointLightColors[0].y * 0.1, pointLightColors[0].z * 0.1);
    glUniform3f(glGetUniformLocation(programId, "pointLights[2].diffuse"), pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);
    glUniform3f(glGetUniformLocation(programId, "pointLights[2].specular"), pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);

    glUniform1f(glGetUniformLocation(programId, "pointLights[2].constant"), 1.0f);
    glUniform1f(glGetUniformLocation(programId, "pointLights[2].linear"), linear);
    glUniform1f(glGetUniformLocation(programId, "pointLights[2].quadratic"), quaratic);
    // Point light 4
    glUniform3f(glGetUniformLocation(programId, "pointLights[3].position"), pointLightPositions[3].x, pointLightPositions[3].y, pointLightPositions[3].z);
    glUniform3f(glGetUniformLocation(programId, "pointLights[3].ambient"), pointLightColors[0].x * 0.1, pointLightColors[0].y * 0.1, pointLightColors[0].z * 0.1);
    glUniform3f(glGetUniformLocation(programId, "pointLights[3].diffuse"), pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);
    glUniform3f(glGetUniformLocation(programId, "pointLights[3].specular"), pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);

    glUniform1f(glGetUniformLocation(programId, "pointLights[3].constant"), 1.0f);
    glUniform1f(glGetUniformLocation(programId, "pointLights[3].linear"), linear);
    glUniform1f(glGetUniformLocation(programId, "pointLights[3].quadratic"), quaratic);
}

void setupFlashLight(GLuint programId, glm::vec3 position, glm::vec3 direction) {
    GLfloat linear = 0.7f;
    GLfloat quaratic = 1.8f;
    glUniform3fv(glGetUniformLocation(shaderProgram.program, "spotLight.position"), 1, glm::value_ptr(position));
    glUniform3fv(glGetUniformLocation(programId, "spotLight.direction"), 1, glm::value_ptr(direction));
    glUniform3f(glGetUniformLocation(programId, "spotLight.ambient"), 0.0f, 0.0f, 0.0f);
    glUniform3f(glGetUniformLocation(programId, "spotLight.diffuse"), 1.0f, 1.0f, 1.0f);
    glUniform3f(glGetUniformLocation(programId, "spotLight.specular"), 1.0f, 1.0f, 1.0f);
    glUniform1f(glGetUniformLocation(programId, "spotLight.constant"), 1.0f);
    glUniform1f(glGetUniformLocation(programId, "spotLight.linear"), linear);
    glUniform1f(glGetUniformLocation(programId, "spotLight.quadratic"), quaratic);
    glUniform1f(glGetUniformLocation(programId, "spotLight.cutOff"), glm::cos(glm::radians(12.5f)));
    glUniform1f(glGetUniformLocation(programId, "spotLight.outerCutOff"), glm::cos(glm::radians(15.0f)));
}

void fog(GLuint programId) {
    glUniform4fv(glGetUniformLocation(programId, "fog.color"), 1, glm::value_ptr(FOG_COLOR));
    glUniform1f(glGetUniformLocation(programId, "fog.start"), FOG_START);
    glUniform1f(glGetUniformLocation(programId, "fog.end"), FOG_END);
    glUniform1f(glGetUniformLocation(programId, "fog.density"), FOG_DENSITY);
    glUniform1i(glGetUniformLocation(programId, "fog.type"), FOG_TYPE);
}

/**
 * draw scene objects by camera
 */
void drawWindowContents() {

    glm::mat4 viewMatrix = sceneObjects.camera->getViewMatrix();
    glm::mat4 projectionMatrix = glm::perspective(60.0f, sceneState.windowWidth / (float) sceneState.windowHeight, 0.1f, 10.0f);

    //fog
    glUseProgram(shaderProgram.program);
    fog(shaderProgram.program);
    CHECK_GL_ERROR();
    glUseProgram(0);
    glUseProgram(skyboxShader.program);
    fog(skyboxShader.program);
    CHECK_GL_ERROR();
    glUseProgram(0);

    //lights
    glm::vec3 pointLightPositions[GLOW_WORM_COUNT];
    glm::vec3 pointLightColors[GLOW_WORM_COUNT];
    for (int i = 0; i < GLOW_WORM_COUNT; i++) {
        pointLightPositions[i] = ((GlowWormObject*) sceneObjects.glowWorms.at(i))->position;
        pointLightColors[i] = ((GlowWormObject*) sceneObjects.glowWorms.at(i))->color;
    }
    glUseProgram(shaderProgram.program);
    setupLights(shaderProgram.program, pointLightPositions, pointLightColors);
    setupFlashLight(shaderProgram.program, sceneObjects.camera->getPosition(), sceneObjects.camera->getFront());
    glUniform1f(glGetUniformLocation(shaderProgram.program, "time"), sceneObjects.explosion->currentTime);
    glUniform1i(glGetUniformLocation(shaderProgram.program, "trippy"), 0);
    glUseProgram(0);
    CHECK_GL_ERROR();

    auto gw = sceneObjects.glowWorms[0];
    drawSkybox(sceneObjects.skyBox, viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();

    // draw ground
    drawGround(sceneObjects.ground, viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();

    //draw house
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    int id = 3;
    glStencilFunc(GL_ALWAYS, id, -1);
    drawHouse(sceneObjects.house, viewMatrix, projectionMatrix);
    glStencilFunc(GL_ALWAYS, id, -1);
    glDisable(GL_STENCIL_TEST);
    CHECK_GL_ERROR();

    //draw cart
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    id = 1;
    glStencilFunc(GL_ALWAYS, id, -1);
    drawCart(sceneObjects.cart, viewMatrix, projectionMatrix);
    glStencilFunc(GL_ALWAYS, id, -1);
    glDisable(GL_STENCIL_TEST);
    CHECK_GL_ERROR();

    // draw trees
    for (auto tree : sceneObjects.trees) {
        if (((TreeObject*) tree)->destroyed == false)
            drawTree((TreeObject*) tree, viewMatrix, projectionMatrix);
    }
    CHECK_GL_ERROR();

    for (auto tree : sceneObjects.glowWorms) {
        drawGlowWorm((GlowWormObject*) tree, viewMatrix, projectionMatrix);
    }
    CHECK_GL_ERROR();
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    id = 2;
    glStencilFunc(GL_ALWAYS, id, -1);
    drawCube(sceneObjects.cube, viewMatrix, projectionMatrix);
    glStencilFunc(GL_ALWAYS, id, -1);
    glDisable(GL_STENCIL_TEST);
    CHECK_GL_ERROR();

    drawExplosion(sceneObjects.explosion, viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();
}

/**
 * Called to update the display. You should call glutSwapBuffers after all of your rendering to display what you rendered.
 */
void displayCallback() {
    GLbitfield mask = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;
    mask |= GL_STENCIL_BUFFER_BIT;

    glClear(mask);

    drawWindowContents();

    glutSwapBuffers();
}

/**
 * Called whenever the window is resized. The new window size is given, in pixels.
 * This is an opportunity to call glViewport or glScissor to keep up with the change in size.
 * @param newWidth
 * @param newHeight
 */
void reshapeCallback(int newWidth, int newHeight) {

    sceneState.windowWidth = newWidth;
    sceneState.windowHeight = newHeight;

    glViewport(0, 0, (GLsizei) newWidth, (GLsizei) newHeight);
}

/**
 * Are spheres overlaping?
 * @param center1
 * @param radius1
 * @param center2
 * @param radius2
 * @return 
 */
bool spheresIntersection(const glm::vec3 &center1, float radius1, const glm::vec3 &center2, float radius2) {
    float distance = sqrt(pow(center1.x - center2.x, 2) + pow(center1.y - center2.y, 2) + pow(center1.z - center2.z, 2));
    return distance < (radius1 + radius2);
}

void checkCollisions() {
    //camera hiting tree
    for (auto tree : sceneObjects.trees) {
        if (((TreeObject *) tree)->destroyed == false) {
            if (spheresIntersection(
                    sceneObjects.camera->getPosition(), 0.002f,
                    ((TreeObject *) tree)->position,
                    ((TreeObject *) tree)->size) == true) {
                ((TreeObject *) tree)->destroyed = true;
                std::cout << "tree died" << std::endl;
            }
        }
    }
    if (sceneObjects.camera->getPosition().x > 0.9f) {
        sceneObjects.camera->setPosition(
                glm::vec3(0.9f, sceneObjects.camera->getPosition().y, sceneObjects.camera->getPosition().z)
                );
    }
    if (sceneObjects.camera->getPosition().x < -0.9f) {
        sceneObjects.camera->setPosition(
                glm::vec3(-0.9f, sceneObjects.camera->getPosition().y, sceneObjects.camera->getPosition().z)
                );
    }
    if (sceneObjects.camera->getPosition().z > 0.9f) {
        sceneObjects.camera->setPosition(
                glm::vec3(sceneObjects.camera->getPosition().x, sceneObjects.camera->getPosition().y, 0.9f)
                );
    }
    if (sceneObjects.camera->getPosition().z < -0.9f) {
        sceneObjects.camera->setPosition(
                glm::vec3(sceneObjects.camera->getPosition().x, sceneObjects.camera->getPosition().y, -0.9f)
                );
    }
}

void updateObjects(float elapsedTime) {
    GLfloat timeDelta = elapsedTime - sceneObjects.cart->currentTime;
    ((GlowWormObject*) sceneObjects.glowWorms[0])->currentTime = elapsedTime;
    sceneObjects.explosion->currentTime = elapsedTime;
    if (sceneObjects.cart->speed != 0.0f) {
        sceneObjects.cart->currentTime = elapsedTime;
        float curveParamT = sceneObjects.cart->speed * (sceneObjects.cart->currentTime - sceneObjects.cart->startTime);
        sceneObjects.cart->position = glm::vec3(0.0f) + evaluateClosedCurve(curveData, curveSize, curveParamT);
        sceneObjects.cart->direction = glm::normalize(evaluateClosedCurve_1stDerivative(curveData, curveSize, curveParamT));
        sceneObjects.camera->setPosition(
                glm::vec3(sceneObjects.cart->position.x, sceneObjects.cart->position.y + 0.1f, sceneObjects.cart->position.z)
                );
        sceneObjects.camera->setFront(sceneObjects.cart->position);
    }

}

void timerCallback(int) {

    // update scene time
    GLfloat oldTime = sceneState.elapsedTime;
    sceneState.elapsedTime = 0.001f * (float) glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds
    GLfloat timeDelta = sceneState.elapsedTime - oldTime;

    //std::cout << timeDelta << std::endl;

    if (sceneState.keyMap[KEY_A] == true && sceneState.freeCameraMode)
        sceneObjects.camera->processKeyboard(LEFT, timeDelta);

    if (sceneState.keyMap[KEY_D] == true && sceneState.freeCameraMode)
        sceneObjects.camera->processKeyboard(RIGHT, timeDelta);

    if (sceneState.keyMap[KEY_W] == true && sceneState.freeCameraMode)
        sceneObjects.camera->processKeyboard(FORWARD, timeDelta);

    if (sceneState.keyMap[KEY_S] == true && sceneState.freeCameraMode)
        sceneObjects.camera->processKeyboard(BACKWARD, timeDelta);

    checkCollisions();

    // update objects in the scene
    updateObjects(sceneState.elapsedTime);

    glutTimerFunc(33, timerCallback, 0);

    glutPostRedisplay();
}

void passiveMouseMotionCallback(int mouseX, int mouseY) {

    if (mouseY != sceneState.windowHeight / 2) {
        GLfloat cameraElevationAngleDelta = 0.2f * (mouseY - sceneState.windowHeight / 2);
        sceneObjects.camera->processMouseMovement(0.0f, cameraElevationAngleDelta);

        glutWarpPointer(sceneState.windowWidth / 2, sceneState.windowHeight / 2);
        glutPostRedisplay();
    }
    if (mouseX != sceneState.windowWidth / 2) {
        GLfloat cameraHorizontalAngleDelta = 0.2f * (mouseX - sceneState.windowWidth / 2);
        sceneObjects.camera->processMouseMovement(cameraHorizontalAngleDelta, 0.0f);

        glutWarpPointer(sceneState.windowHeight / 2, sceneState.windowWidth / 2);
        glutPostRedisplay();
    }
}

void mouseCallback(int buttonPressed, int buttonState, int mouseX, int mouseY) {
    if ((buttonPressed == GLUT_LEFT_BUTTON) && (buttonState == GLUT_DOWN)) {
        unsigned char id = 0;
        glReadPixels(mouseX, WINDOW_HEIGHT - mouseY, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, &id);
        if (id == 1) {
            printf("It is pallet Jack!!!\n");
            if (sceneObjects.cart->speed == CART_SPEED) {
                sceneObjects.cart->speed = 0.0f;
            } else {
                sceneObjects.cart->speed = CART_SPEED;
            }

        } else if (id == 2) {
            printf("It is cube\n");
        } else if (id == 3) {
            printf("It is house\n");
        } else {
            printf("Clicked somewhere to scene\n");
        }
    }
}

// Called whenever a key on the keyboard was pressed. The key is given by the ''key''
// parameter, which is in ASCII. It's often a good idea to have the escape key
// (ASCII value 27) call glutLeaveMainLoop() to exit the program.

void keyboardCallback(unsigned char keyPressed, int mouseX, int mouseY) {

    switch (keyPressed) {
        case 27:
#ifndef __APPLE__
            glutLeaveMainLoop();
#else
            exit(0);
#endif
            break;
        case 'a':
            sceneState.keyMap[KEY_A] = true;
            break;
        case 'd':
            sceneState.keyMap[KEY_D] = true;
            break;
        case 'w':
            sceneState.keyMap[KEY_W] = true;
            break;
        case 's':
            sceneState.keyMap[KEY_S] = true;
            break;
        default:
            ;
    }
}

void keyboardUpCallback(unsigned char keyPressed, int mouseX, int mouseY) {

    switch (keyPressed) {
        case 'a':
            sceneState.keyMap[KEY_A] = false;
            break;
        case 'd':
            sceneState.keyMap[KEY_D] = false;
            break;
        case 'w':
            sceneState.keyMap[KEY_W] = false;
            break;
        case 's':
            sceneState.keyMap[KEY_S] = false;
            break;
        case 'r':
            restartScene();
            break;
        case 'c':
            sceneState.freeCameraMode = !sceneState.freeCameraMode;
            if (sceneState.freeCameraMode == true) {
                //sceneObjects.camera->reset();
                glutPassiveMotionFunc(passiveMouseMotionCallback);
                glutWarpPointer(sceneState.windowWidth / 2, sceneState.windowHeight / 2);
            } else {
                glutPassiveMotionFunc(NULL);
            }
            break;
        case '1':
            sceneState.freeCameraMode = false;
            glutPassiveMotionFunc(NULL);
            sceneObjects.camera->setView1();
            break;
        case '2':
            sceneState.freeCameraMode = false;
            glutPassiveMotionFunc(NULL);
            sceneObjects.camera->setView2();
            break;
        default:
            ;
    }
}

void specialKeyboardCallback(int specKeyPressed, int mouseX, int mouseY) {
    switch (specKeyPressed) {
        case GLUT_KEY_UP:
            std::cout << "Up" << std::endl;
            sceneObjects.cart->speed = CART_SPEED;
            break;
        case GLUT_KEY_DOWN:
            std::cout << "Down" << std::endl;
            sceneObjects.cart->speed = 0.f;
            break;
        case GLUT_KEY_F1:
            sceneState.keyMap[KEY_FLIGHT] = true;
            if (sceneObjects.camera->isFlying()) {
                sceneObjects.camera->setFlying(false);
            } else {
                sceneObjects.camera->setFlying(true);
            }

            break;
    }
}

void specialKeyboardUpCallback(int specKeyPressed, int mouseX, int mouseY) {
    switch (specKeyPressed) {
        case GLUT_KEY_F1:
            sceneState.keyMap[KEY_FLIGHT] = false;
            break;
    }
}

void funcname(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, void* userParam) {
    fprintf(stderr, "%s\n", message);
}

/**
 * Called after the window and OpenGL are initialized. Called exactly once, before the main loop.
 */
void initializeApplication() {

    // initialize random seed
    srand((unsigned int) time(NULL));

    // initialize OpenGL
    glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
    glClearStencil(0); // this is the default value
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(funcname, nullptr);
    glPointSize(5.0f);

    useLighting = true;

    // initialize shaders
    initializeShaderPrograms();
    // create geometry for all models used
    initializeModels();

    sceneObjects.ground = NULL;
    sceneObjects.house = NULL;

    restartScene();
}

void finalizeApplication(void) {

    cleanUpObjects();

    delete sceneObjects.ground;
    delete sceneObjects.house;
    sceneObjects.ground = NULL;
    sceneObjects.house = NULL;

    // delete buffers
    cleanupModels();

    // delete shaders
    cleanupShaderPrograms();
}

int main(int argc, char** argv) {

    // initialize windowing system
    glutInit(&argc, argv);

#ifndef __APPLE__
    glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
#else
    glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
#endif

    // initial window size
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    glutCreateWindow(WINDOW_TITLE);

    glutDisplayFunc(displayCallback);
    // register callback for change of window size
    glutReshapeFunc(reshapeCallback);

    //mouse callback
    glutMouseFunc(mouseCallback);
    // register callbacks for keyboard
    glutKeyboardFunc(keyboardCallback);
    glutKeyboardUpFunc(keyboardUpCallback);
    glutSpecialFunc(specialKeyboardCallback);
    glutSpecialUpFunc(specialKeyboardUpCallback);

    glutTimerFunc(33, timerCallback, 0);

    // initialize GL, devil etc.
    if (!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
        pgr::dieWithError("pgr init failed, required OpenGL not supported?");

    initializeApplication();

    glutMainLoop();

#ifndef __APPLE__
    glutCloseFunc(finalizeApplication);
#else
    glutWMCloseFunc(finalizeApplication);
#endif

    return 0;
}