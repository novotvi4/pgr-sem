#pragma once
#include "pgr.h"

#define WINDOW_WIDTH   750
#define WINDOW_HEIGHT  750
#define WINDOW_TITLE   "Scene"

// keys used in the key map

enum {
    KEY_A, KEY_D, KEY_W, KEY_S, KEY_FLIGHT, KEY_START, KEY_STOP, KEYS_COUNT
};

#define TREES_COUNT_MIN  8
#define TREES_COUNT_MAX  16

#define GROUND_SIZE     1.0f

#define HOUSE_SIZE     0.5f
#define HOUSE_POSITION glm::vec3(0.f, 0.15f, 0.5f)

#define CART_SPEED    0.4f
#define CART_SIZE     0.05f
#define CART_POSITION glm::vec3(0.8f, -0.06f, -0.8f)

#define SCENE_WIDTH  1.0f
#define SCENE_HEIGHT 1.0f
#define SCENE_DEPTH  1.0f

#define GLOW_WORM_SPEED  0.2f
#define GLOW_WORM_COUNT  4


#define FOG_COLOR  glm::vec4(0.8f,0.8f,1.0f,1.0f)
#define FOG_START   0.4f
#define FOG_END     0.8f
#define FOG_DENSITY 0.6f
#define FOG_TYPE    1

#define CUBE_SIZE     0.05f
#define CUBE_POSITION glm::vec3(0.4f,-0.04f,0.6f)

#define BANNER_POSITION glm::vec3(-0.4f, 0.0f, 0.4f)
#define BANNER_SIZE     0.04f
#define BANNER_SPEED    1.04f


extern glm::vec3 wormsColors[];
extern int groundTrianglesCount;
extern std::vector<GLfloat> groundVertices;
extern std::vector<GLfloat> skyboxVertices;
extern std::vector<GLfloat> cubeVertices;
extern const int curveSize;
extern glm::vec3 curveData[];
extern std::vector<GLfloat> bannerVertices;
extern std::vector<GLfloat> bannerIndices;

extern const int explosionNumQuadVertices;
extern const std::vector<GLfloat> explosionVertexData;
