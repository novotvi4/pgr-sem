#version 330 core  

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform float time;
uniform int trippy;

in vec3 position;    
in vec3 color;
in vec3 normal;             // vertex normal
in vec2 texCoord;           // incoming texture coordinates  
in int  noTex; 

out vec2 texCoord_v;
out vec4 color_v;
out vec3 normal_v;
out vec3 fragPosition;
out vec4 vEyeSpacePos;
flat out int noTexture;

void main() {
    mat4 pvm = projectionMatrix * viewMatrix * modelMatrix;
    gl_Position = pvm * vec4(position, 1.0);
    color_v = vec4(color, 1.0);
    
    if(trippy == 1){
        texCoord_v = vec2(texCoord.x,texCoord.y+time);
    }else{
        texCoord_v = texCoord;
    }
    normal_v = mat3(transpose(inverse(modelMatrix))) * normal;
    fragPosition = vec3(modelMatrix * vec4(position, 1.0f));

    vec4 vEyeSpacePosVertex = viewMatrix * modelMatrix  * vec4(position, 1.0);
    vEyeSpacePos = vEyeSpacePosVertex;    
}