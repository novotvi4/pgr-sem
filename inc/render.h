#pragma once

static bool useLighting = true;

typedef struct MeshGeometry {
    GLuint vertexBufferObject; // identifier for the vertex buffer object
    GLuint elementBufferObject; // identifier for the element buffer object
    GLuint vertexArrayObject; // identifier for the vertex array object
    unsigned int numTriangles; // number of triangles in the mesh

    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    float shininess;
    GLuint texture;
    GLuint specularTexture;

} MeshGeometry;

typedef struct PointGeometry {
    GLuint vertexBufferObject;
    GLuint vertexArrayObject;
} PointGeometry;

typedef struct Object {
    glm::vec3 position;
    glm::vec3 direction;
    float speed;
    float size;

    bool destroyed;

    float startTime;
    float currentTime;

} Object;

typedef struct TreeObject : public Object {
    float rotationDegrees;
} TreeObject;

typedef struct GroundObject : public Object {
} GroundObject;

typedef struct HouseObject : public Object {
} HouseObject;

typedef struct CartObject : public Object {
} CartObject;

typedef struct SkyBoxObject : public Object {
} SkyBoxObject;

typedef struct CubeObject : public Object {
} CubeObject;

typedef struct BannerObject : public Object {
} BannerObject;

typedef struct ExplosionObject : public Object {
    float frameDuration;
} ExplosionObject;

typedef struct GlowWormObject : public Object {

    GlowWormObject(float r = 1.0f, float g = 1.0f, float b = 1.0f) : color(r, g, b) {
        position = glm::vec3(0.0f, 0.0f, 0.0f);
    }
    glm::vec3 color;
} GlowWormObject;

typedef struct ShaderProgram {
    GLuint program;
    GLint projectionMatrixLocation;
    GLint viewMatrixLocation;
    GLint modelMatrixLocation;
} ShaderProgram;

typedef struct _commonShaderProgram : public ShaderProgram {
    // vertex attributes locations
    GLint positionLocation; // = -1;
    GLint colorLocation; // = -1;
    GLint normalLocation; // = -1;
    GLint texCoordLocation; // = -1;
    // uniforms locations
    GLint normalMatrixLocation; // = -1;  inverse transposed Mmatrix

    GLint timeLocation; // = -1; elapsed time in seconds

    // material 
    GLint diffuseLocation; // = -1;
    GLint specularLocation; // = -1;
    GLint shininessLocation; // = -1;

} SCommonShaderProgram;

typedef struct _plShaderProgram : public ShaderProgram {
    GLint positionLocation; // = -1;
    GLint colorLocation; // = -1;

} SPointLightShaderProgram;

typedef struct _ssbshd : public ShaderProgram {
} SSbShd;

typedef struct _epxjpvrj : public ShaderProgram {
    GLint positionLocation;
    GLint texCoordLocation;
    GLint PVMmatrixLocation;
    GLint VmatrixLocation;
    GLint timeLocation;
    GLint texSamplerLocation;
    GLint frameDurationLocation;
} ExShaderProgram;

void drawHouse(HouseObject* house, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);
void drawTree(TreeObject* tree, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);
void drawGround(GroundObject* ground, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);
void drawGlowWorm(GlowWormObject* glowWorm, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);
void drawCart(CartObject* ground, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);
void drawSkybox(SkyBoxObject* sko, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
void drawCube(CubeObject* sko, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
void drawBanner(BannerObject* banner, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
void drawExplosion(ExplosionObject* explosion, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);

void initializeShaderPrograms();
void cleanupShaderPrograms();

void initializeModels();
void cleanupModels();
bool loadSingleMesh(const std::string &fileName, SCommonShaderProgram& shader, MeshGeometry** geometry);
