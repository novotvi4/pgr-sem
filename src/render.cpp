#include <iostream>
#include <assimp/material.h>
#include "pgr.h"
#include "render.h"
#include "data.h"
#include "helpers.h"

MeshGeometry* treeGeometry = NULL;
MeshGeometry* groundGeometry = NULL;
MeshGeometry* houseGeometry = NULL;
MeshGeometry* cartGeometry = NULL;
MeshGeometry* skyBoxGeometry = NULL;
MeshGeometry* cubeGeometry = NULL;
MeshGeometry* explosionGeometry = NULL;
PointGeometry* glowWormGeometry = NULL;

const char* TREE_MODEL_NAME = "data/tree.obj";
const char* HOUSE_MODEL_NAME = "data/house_obj.obj";
const char* CART_MODEL_NAME = "data/palletjack.obj";

SCommonShaderProgram shaderProgram;
SPointLightShaderProgram plShaderProgram;
SSbShd skyboxShader;
ExShaderProgram explosionShaderProgram;

void setTransformUniforms(ShaderProgram sp, const glm::mat4 &modelMatrix, const glm::mat4 &viewMatrix, const glm::mat4 &projectionMatrix) {
    glUniformMatrix4fv(sp.projectionMatrixLocation, 1, GL_FALSE, glm::value_ptr(projectionMatrix));
    glUniformMatrix4fv(sp.viewMatrixLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix));
    glUniformMatrix4fv(sp.modelMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelMatrix));
}

void setMaterialUniforms(float shininess, GLuint diffuseTexture, GLuint specularTexture) {
    glUniform1f(shaderProgram.shininessLocation, shininess);

    glUniform1i(shaderProgram.diffuseLocation, 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, diffuseTexture);

    glUniform1i(shaderProgram.specularLocation, 1);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, specularTexture);

}

void setLightsUniforms(GlowWormObject* gw) {
    glUniform3fv(plShaderProgram.colorLocation, 1, glm::value_ptr(gw->color));
    //std::cout << shaderProgram.colorLocation << std::endl;
    //printVec3(gw->color, "color: ");
    //std::cout << gw->color.r<<" "<< gw->color.g << " "<< gw->color.b << std::endl;
}

/**
 * @param house
 * @param viewMatrix
 * @param projectionMatrix
 */
void drawHouse(HouseObject* house, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {

    glUseProgram(shaderProgram.program);

    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), house->position);
    modelMatrix = glm::rotate(modelMatrix, 180.0f, glm::vec3(0, 1, 0));
    modelMatrix = glm::scale(modelMatrix, glm::vec3(house->size));

    // setting matrices to the vertex & fragment shader
    setTransformUniforms(shaderProgram, modelMatrix, viewMatrix, projectionMatrix);
    setMaterialUniforms(
            houseGeometry->shininess,
            houseGeometry->texture,
            houseGeometry->specularTexture
            );


    glBindVertexArray(houseGeometry->vertexArrayObject);
    glDrawElements(GL_TRIANGLES, houseGeometry->numTriangles * 3, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}

/**
 * @param tree
 * @param viewMatrix
 * @param projectionMatrix
 */
void drawTree(TreeObject* tree, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {

    glUseProgram(shaderProgram.program);

    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), tree->position);
    modelMatrix = glm::scale(modelMatrix, glm::vec3(tree->size));
    modelMatrix = glm::rotate(modelMatrix, tree->rotationDegrees, glm::vec3(0, 0, 1));

    // setting matrices to the vertex & fragment shader
    setTransformUniforms(shaderProgram, modelMatrix, viewMatrix, projectionMatrix);
    setMaterialUniforms(
            treeGeometry->shininess,
            0,
            0
            );

    glBindVertexArray(treeGeometry->vertexArrayObject);
    glDrawElements(GL_TRIANGLES, treeGeometry->numTriangles * 3, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}

/**
 * @param ground
 * @param viewMatrix
 * @param projectionMatrix
 */
void drawGround(GroundObject* ground, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {

    glUseProgram(shaderProgram.program);

    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), ground->position);
    modelMatrix = glm::rotate(modelMatrix, 0.0f, glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, glm::vec3(ground->size, ground->size, ground->size));

    // setting matrices to the vertex & fragment shader
    setTransformUniforms(shaderProgram, modelMatrix, viewMatrix, projectionMatrix);
    CHECK_GL_ERROR();
    setMaterialUniforms(
            0.3f,
            0,
            0
            );
    glUniform1i(glGetUniformLocation(shaderProgram.program, "noTex"), 1);

    CHECK_GL_ERROR();

    glBindVertexArray(groundGeometry->vertexArrayObject);
    glDrawArrays(GL_TRIANGLES, 0, groundGeometry->numTriangles * 3);

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}

/**
 * @param cart
 * @param viewMatrix
 * @param projectionMatrix
 */
void drawCart(CartObject* cart, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {

    glUseProgram(shaderProgram.program);

    glm::mat4 modelMatrix = alignObject(cart->position, cart->direction, glm::vec3(0.0f, 1.0f, 0.0f));
    modelMatrix = glm::rotate(modelMatrix, 180.0f, glm::vec3(0, 1, 0));
    modelMatrix = glm::scale(modelMatrix, glm::vec3(cart->size));

    // setting matrices to the vertex & fragment shader
    setTransformUniforms(shaderProgram, modelMatrix, viewMatrix, projectionMatrix);
    setMaterialUniforms(
            cartGeometry->shininess,
            cartGeometry->texture,
            cartGeometry->specularTexture
            );

    glBindVertexArray(cartGeometry->vertexArrayObject);
    glDrawElements(GL_TRIANGLES, cartGeometry->numTriangles * 3, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}

/**
 * @param glowWorm
 * @param viewMatrix
 * @param projectionMatrix
 */
void drawGlowWorm(GlowWormObject* glowWorm, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {

    glUseProgram(plShaderProgram.program);
    glowWorm->position.z -= (float) rand() / RAND_MAX / 800.0f;
    glowWorm->position.x += (float) rand() / RAND_MAX / 800.0f;

    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), glowWorm->position);
    modelMatrix = glm::rotate(modelMatrix, 180.0f, glm::vec3(0, 1, 0));
    modelMatrix = glm::scale(modelMatrix, glm::vec3(glowWorm->size));

    // setting matrices to the vertex & fragment shader
    setTransformUniforms(plShaderProgram, modelMatrix, viewMatrix, projectionMatrix);
    setLightsUniforms(glowWorm);

    glBindVertexArray(glowWormGeometry->vertexArrayObject);
    glDrawArrays(GL_POINTS, 0, 1);

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}

/**
 * @param sko
 * @param viewMatrix
 * @param projectionMatrix
 */
void drawSkybox(SkyBoxObject* sko, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    //skybox
    glDepthMask(GL_FALSE);
    glUseProgram(skyboxShader.program); //sceneObjects.camera.getZoom()
    glUniformMatrix4fv(glGetUniformLocation(skyboxShader.program, "view"), 1, GL_FALSE, glm::value_ptr(viewMatrix));
    glUniformMatrix4fv(glGetUniformLocation(skyboxShader.program, "projection"), 1, GL_FALSE, glm::value_ptr(projectionMatrix));
    // skybox cube
    glBindVertexArray(skyBoxGeometry->vertexArrayObject);
    glActiveTexture(GL_TEXTURE0);
    glUniform1i(glGetUniformLocation(skyboxShader.program, "skybox"), 0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skyBoxGeometry->texture);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
    glUseProgram(0);
    glDepthMask(GL_TRUE);
}

void drawCube(CubeObject* cube, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) {
    glUseProgram(shaderProgram.program);
    glUniform1i(glGetUniformLocation(shaderProgram.program, "trippy"), 1);
    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), cube->position);
    modelMatrix = glm::rotate(modelMatrix, 0.0f, glm::vec3(0, 0, 1));
    modelMatrix = glm::scale(modelMatrix, glm::vec3(cube->size, cube->size, cube->size));

    // setting matrices to the vertex & fragment shader
    setTransformUniforms(shaderProgram, modelMatrix, viewMatrix, projectionMatrix);
    setMaterialUniforms(32.0f, cubeGeometry->texture, cubeGeometry->specularTexture);
    //std::cout << "cube diff " << cubeGeometry->texture << std::endl;
    //std::cout << "cube spec " << cubeGeometry->specularTexture << std::endl;
    // cube
    glBindVertexArray(cubeGeometry->vertexArrayObject);
    glDrawElements(GL_TRIANGLES, cubeGeometry->numTriangles * 3, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glUseProgram(0);
}

void cleanupShaderPrograms(void) {

    pgr::deleteProgramAndShaders(shaderProgram.program);
    pgr::deleteProgramAndShaders(plShaderProgram.program);

}

void initGroundGeometry(SCommonShaderProgram &shader, MeshGeometry **geometry) {

    *geometry = new MeshGeometry;

    glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
    glBindVertexArray((*geometry)->vertexArrayObject);

    glGenBuffers(1, &((*geometry)->vertexBufferObject));
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, groundVertices.size() * sizeof (GLfloat), groundVertices.data(), GL_STATIC_DRAW);
    CHECK_GL_ERROR();

    glEnableVertexAttribArray(shader.positionLocation);
    // vertices of triangles - start at the beginning of the array
    glVertexAttribPointer(shader.positionLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);
    CHECK_GL_ERROR();
    glEnableVertexAttribArray(shader.colorLocation);
    // colors of vertices start after the positions
    glVertexAttribPointer(shader.colorLocation, 3, GL_FLOAT, GL_FALSE, 3 * sizeof (float), (void*) (groundTrianglesCount * 3 * 3 * sizeof (float)));
    CHECK_GL_ERROR();
    glEnableVertexAttribArray(shader.normalLocation);
    // colors of vertices start after the positions
    glVertexAttribPointer(shader.normalLocation, 3, GL_FLOAT, GL_FALSE, 3 * sizeof (float), (void*) (2 * (groundTrianglesCount * 3 * 3 * sizeof (float))));
    CHECK_GL_ERROR();
    glBindVertexArray(0);

    (*geometry)->ambient = glm::vec3(0.1f, 0.1f, 0.1f);
    (*geometry)->diffuse = glm::vec3(0.5f, 0.6f, 0.5f);
    (*geometry)->specular = glm::vec3(0.1f, 0.1f, 0.1f);
    (*geometry)->shininess = 32.0f;

    (*geometry)->numTriangles = groundTrianglesCount;
}

void initGlowWorm(SPointLightShaderProgram &shader, PointGeometry **geometry) {

    *geometry = new PointGeometry;

    glGenBuffers(1, &((*geometry)->vertexBufferObject));
    glGenVertexArrays(1, &((*geometry)->vertexArrayObject));

    glBindVertexArray((*geometry)->vertexArrayObject);
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
    const float lol[] = {0.0f, 0.0f, 0.0f};
    glBufferData(GL_ARRAY_BUFFER, sizeof (lol), lol, GL_STATIC_DRAW);
    CHECK_GL_ERROR();

    glVertexAttribPointer(shader.positionLocation, 3, GL_FLOAT, GL_FALSE, 3 * sizeof (float), (void *) 0);
    glEnableVertexAttribArray(shader.positionLocation);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    CHECK_GL_ERROR();
}

void initSkyBoxGeometry(SSbShd &shader, MeshGeometry **geometry) {

    *geometry = new MeshGeometry;
    glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
    glGenBuffers(1, &((*geometry)->vertexBufferObject));
    glBindVertexArray((*geometry)->vertexArrayObject);
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, skyboxVertices.size() * sizeof (GLfloat), skyboxVertices.data(), GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof (GLfloat), (GLvoid*) 0);
    glBindVertexArray(0);

    // Cubemap (Skybox)
    std::vector<const GLchar*> faces;
    faces.push_back("data/skybox/right.jpg");
    faces.push_back("data/skybox/left.jpg");
    faces.push_back("data/skybox/top.jpg");
    faces.push_back("data/skybox/bottom.jpg");
    faces.push_back("data/skybox/front.jpg");
    faces.push_back("data/skybox/back.jpg");
    (*geometry)->texture = loadCubemap(faces);
    CHECK_GL_ERROR();

}

void initBannerGeometry(ExShaderProgram &shader, MeshGeometry **geometry) {

    *geometry = new MeshGeometry;

    (*geometry)->texture = pgr::createTexture("data/digits.png");

    glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
    glBindVertexArray((*geometry)->vertexArrayObject);

    glGenBuffers(1, &((*geometry)->vertexBufferObject));
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, explosionVertexData.size() * sizeof (GLfloat), explosionVertexData.data(), GL_STATIC_DRAW);

    CHECK_GL_ERROR();
    glEnableVertexAttribArray(explosionShaderProgram.positionLocation);
    glVertexAttribPointer(explosionShaderProgram.positionLocation, 3, GL_FLOAT, GL_FALSE, 5 * sizeof (GLfloat), 0);
    CHECK_GL_ERROR();
    glEnableVertexAttribArray(explosionShaderProgram.texCoordLocation);
    glVertexAttribPointer(explosionShaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof (GLfloat), (void*) (3 * sizeof (GLfloat)));

    glBindVertexArray(0);

    (*geometry)->numTriangles = explosionNumQuadVertices;
    CHECK_GL_ERROR();

}

void drawExplosion(ExplosionObject* explosion, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {

    glUseProgram(explosionShaderProgram.program);

    glm::mat4 billboardRotationMatrix = glm::mat4(
            viewMatrix[0],
            viewMatrix[1],
            viewMatrix[2],
            glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)
            );
    // inverse view rotation
    billboardRotationMatrix = glm::transpose(billboardRotationMatrix);

    glm::mat4 matrix = glm::translate(glm::mat4(1.0f), explosion->position);
    matrix = glm::scale(matrix, glm::vec3(explosion->size));
    matrix = matrix*billboardRotationMatrix; // make billboard to face the camera

    glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * matrix;
    glUniformMatrix4fv(explosionShaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix)); // model-view-projection
    glUniformMatrix4fv(explosionShaderProgram.VmatrixLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix)); // view
    glUniform1f(explosionShaderProgram.timeLocation, explosion->currentTime - explosion->startTime);
    glUniform1i(explosionShaderProgram.texSamplerLocation, 1);
    glUniform1f(explosionShaderProgram.frameDurationLocation, explosion->frameDuration);

    glBindVertexArray(explosionGeometry->vertexArrayObject);
    //std::cout << " expl tex: " << explosionGeometry->texture << std::endl;
    glBindTexture(GL_TEXTURE_2D, explosionGeometry->texture);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, explosionGeometry->numTriangles);

    glBindVertexArray(0);
    glUseProgram(0);

    return;
}

/**
 * Initialize vertex buffers and vertex arrays for all objects. 
 */
void initializeModels() {

    // load tree model from external file
    if (loadSingleMesh(TREE_MODEL_NAME, shaderProgram, &treeGeometry) != true) {
        std::cerr << "initializeModels(): Asteroid model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();

    // load house model from external file
    if (loadSingleMesh(HOUSE_MODEL_NAME, shaderProgram, &houseGeometry) != true) {
        std::cerr << "initializeModels(): House model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();

    // load cart model from external file
    if (loadSingleMesh(CART_MODEL_NAME, shaderProgram, &cartGeometry) != true) {
        std::cerr << "initializeModels(): Cart model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();

    // fill MeshGeometry structure for ground object
    initGroundGeometry(shaderProgram, &groundGeometry);
    CHECK_GL_ERROR();

    initGlowWorm(plShaderProgram, &glowWormGeometry);
    CHECK_GL_ERROR();

    initSkyBoxGeometry(skyboxShader, &skyBoxGeometry);
    CHECK_GL_ERROR();

    if (loadSingleMesh("data/cube.obj", shaderProgram, &cubeGeometry) != true) {
        std::cerr << "initializeModels(): Cube model loading failed." << std::endl;
    }
    CHECK_GL_ERROR();

    initBannerGeometry(explosionShaderProgram, &explosionGeometry);
    CHECK_GL_ERROR();
}

void initializeShaderPrograms(void) {

    std::vector<GLuint> shaderList;
    shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "../shd/simple.vert"));
    shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "../shd/simple.frag"));

    // create the program with two shaders (fragment and vertex)
    shaderProgram.program = pgr::createProgram(shaderList);
    // get position and color attributes locations
    shaderProgram.positionLocation = glGetAttribLocation(shaderProgram.program, "position");
    shaderProgram.colorLocation = glGetAttribLocation(shaderProgram.program, "color");
    shaderProgram.normalLocation = glGetAttribLocation(shaderProgram.program, "normal");
    shaderProgram.texCoordLocation = glGetAttribLocation(shaderProgram.program, "texCoord");
    // link shader variaables to C struct
    shaderProgram.projectionMatrixLocation = glGetUniformLocation(shaderProgram.program, "projectionMatrix");
    shaderProgram.viewMatrixLocation = glGetUniformLocation(shaderProgram.program, "viewMatrix");
    shaderProgram.modelMatrixLocation = glGetUniformLocation(shaderProgram.program, "modelMatrix");

    shaderProgram.diffuseLocation = glGetUniformLocation(shaderProgram.program, "material.diffuse");
    shaderProgram.specularLocation = glGetUniformLocation(shaderProgram.program, "material.specular");
    shaderProgram.shininessLocation = glGetUniformLocation(shaderProgram.program, "material.shininess");

    //
    //another
    //
    shaderList.clear();
    shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "../shd/point.vert"));
    shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "../shd/point.frag"));
    plShaderProgram.program = pgr::createProgram(shaderList);
    // link shader variaables to C struct
    plShaderProgram.positionLocation = glGetAttribLocation(plShaderProgram.program, "position");
    plShaderProgram.colorLocation = glGetUniformLocation(plShaderProgram.program, "color");
    plShaderProgram.projectionMatrixLocation = glGetUniformLocation(plShaderProgram.program, "projectionMatrix");
    plShaderProgram.viewMatrixLocation = glGetUniformLocation(plShaderProgram.program, "viewMatrix");
    plShaderProgram.modelMatrixLocation = glGetUniformLocation(plShaderProgram.program, "modelMatrix");

    //
    //another
    //
    shaderList.clear();
    shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "../shd/skybox.vert"));
    shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "../shd/skybox.frag"));
    skyboxShader.program = pgr::createProgram(shaderList);

    //
    //another
    //
    shaderList.clear();
    shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "../shd/explosion.vert"));
    shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "../shd/explosion.frag"));
    explosionShaderProgram.program = pgr::createProgram(shaderList);
    // get position and texture coordinates attributes locations
    explosionShaderProgram.positionLocation = glGetAttribLocation(explosionShaderProgram.program, "position");
    explosionShaderProgram.texCoordLocation = glGetAttribLocation(explosionShaderProgram.program, "texCoord");
    // get uniforms locations
    explosionShaderProgram.PVMmatrixLocation = glGetUniformLocation(explosionShaderProgram.program, "PVMmatrix");
    explosionShaderProgram.VmatrixLocation = glGetUniformLocation(explosionShaderProgram.program, "Vmatrix");
    explosionShaderProgram.timeLocation = glGetUniformLocation(explosionShaderProgram.program, "time");
    explosionShaderProgram.texSamplerLocation = glGetUniformLocation(explosionShaderProgram.program, "texSampler");
    explosionShaderProgram.frameDurationLocation = glGetUniformLocation(explosionShaderProgram.program, "frameDuration");

}

void cleanupGeometry(MeshGeometry *geometry) {
    glDeleteVertexArrays(1, &(geometry->vertexArrayObject));
    glDeleteBuffers(1, &(geometry->elementBufferObject));
    glDeleteBuffers(1, &(geometry->vertexBufferObject));
}

void cleanupModels() {
    cleanupGeometry(treeGeometry);
    cleanupGeometry(houseGeometry);
    cleanupGeometry(cartGeometry);
    cleanupGeometry(groundGeometry);
}

/**
 * From asteroids
 * Load mesh using assimp library
 * \param filename [in] file to open/load
 * \param shader [in] vao will connect loaded data to shader
 * \param vbo [out] vertex and normal data |VVVVV...|NNNNN...| (no interleaving)
 * \param eao [out] triangle indices
 * \param vao [out] vao connects data to shader input
 * \param numTriangles [out] how many triangles have been loaded and stored into index array eao
 */
bool loadSingleMesh(const std::string &fileName, SCommonShaderProgram& shader, MeshGeometry** geometry) {
    std::cout << "start loading " << fileName << std::endl;
    Assimp::Importer importer;

    importer.SetPropertyInteger(AI_CONFIG_PP_PTV_NORMALIZE, 1); // Unitize object in size (scale the model to fit into (-1..1)^3)
    // Load asset from the file - you can play with various processing steps
    const aiScene * scn = importer.ReadFile(fileName.c_str(), 0
            | aiProcess_Triangulate // Triangulate polygons (if any).
            | aiProcess_PreTransformVertices // Transforms scene hierarchy into one root with geometry-leafs only. For more see Doc.
            | aiProcess_GenSmoothNormals // Calculate normals per vertex.
            | aiProcess_JoinIdenticalVertices);
    // abort if the loader fails
    if (scn == NULL) {
        std::cerr << "assimp error: " << importer.GetErrorString() << std::endl;
        *geometry = NULL;
        return false;
    }
    // some formats store whole scene (multiple meshes and materials, lights, cameras, ...) in one file, we cannot handle that in our simplified example
    if (scn->mNumMeshes != 1) {
        std::cerr << "this simplified loader can only process files with only one mesh" << std::endl;
        *geometry = NULL;
        return false;
    }
    // in this phase we know we have one mesh in our loaded scene, we can directly copy its data to opengl ...
    const aiMesh * mesh = scn->mMeshes[0];

    *geometry = new MeshGeometry;

    // vertex buffer object, store all vertex positions and normals
    glGenBuffers(1, &((*geometry)->vertexBufferObject));
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, 8 * sizeof (float)*mesh->mNumVertices, 0, GL_STATIC_DRAW); // allocate memory for vertices, normals, and texture coordinates
    // first store all vertices
    glBufferSubData(GL_ARRAY_BUFFER, 0, 3 * sizeof (float)*mesh->mNumVertices, mesh->mVertices);
    // then store all normals
    glBufferSubData(GL_ARRAY_BUFFER, 3 * sizeof (float)*mesh->mNumVertices, 3 * sizeof (float)*mesh->mNumVertices, mesh->mNormals);

    // just texture 0 for now
    float *textureCoords = new float[2 * mesh->mNumVertices]; // 2 floats per vertex
    float *currentTextureCoord = textureCoords;

    // copy texture coordinates
    aiVector3D vect;

    if (mesh->HasTextureCoords(0)) {
        // we use 2D textures with 2 coordinates and ignore the third coordinate
        for (unsigned int idx = 0; idx < mesh->mNumVertices; idx++) {
            vect = (mesh->mTextureCoords[0])[idx];
            *currentTextureCoord++ = vect.x;
            *currentTextureCoord++ = vect.y;
        }
    }

    // finally store all texture coordinates
    glBufferSubData(GL_ARRAY_BUFFER, 6 * sizeof (float)*mesh->mNumVertices, 2 * sizeof (float)*mesh->mNumVertices, textureCoords);

    // copy all mesh faces into one big array (assimp supports faces with ordinary number of vertices, we use only 3 -> triangles)
    unsigned int *indices = new unsigned int[mesh->mNumFaces * 3];
    for (unsigned int f = 0; f < mesh->mNumFaces; ++f) {
        indices[f * 3 + 0] = mesh->mFaces[f].mIndices[0];
        indices[f * 3 + 1] = mesh->mFaces[f].mIndices[1];
        indices[f * 3 + 2] = mesh->mFaces[f].mIndices[2];
    }
    // copy our temporary index array to OpenGL and free the array
    glGenBuffers(1, &((*geometry)->elementBufferObject));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->elementBufferObject);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof (unsigned) * mesh->mNumFaces, indices, GL_STATIC_DRAW);

    delete [] indices;

    // copy the material info to MeshGeometry structure
    const aiMaterial *mat = scn->mMaterials[mesh->mMaterialIndex];
    aiColor3D color;
    aiString name;

    // Get returns: aiReturn_SUCCESS 0 | aiReturn_FAILURE -1 | aiReturn_OUTOFMEMORY -3
    mat->Get(AI_MATKEY_NAME, name); // may be "" after the input mesh processing. Must be aiString type!

    mat->Get(AI_MATKEY_COLOR_DIFFUSE, color);
    (*geometry)->diffuse = glm::vec3(color.r, color.g, color.b);
    printVec3((*geometry)->diffuse, "diffuse of " + fileName + " loaded from " + name.data + " set to: ");

    // mat->Get(AI_MATKEY_COLOR_AMBIENT, color);
    mat->Get(AI_MATKEY_COLOR_AMBIENT, color);
    (*geometry)->ambient = glm::vec3(color.r, color.g, color.b);

    mat->Get(AI_MATKEY_COLOR_SPECULAR, color);
    (*geometry)->specular = glm::vec3(color.r, color.g, color.b);

    float shininess;
    mat->Get(AI_MATKEY_SHININESS, shininess);
    (*geometry)->shininess = shininess / 4.0f; // shininess divisor-not descibed anywhere
    std::cout << " shivwoivnw " << shininess << std::endl;

    (*geometry)->texture = 0;

    // load texture image
    if (mat->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
        // get texture name
        aiString s;
        mat->GetTexture(aiTextureType_DIFFUSE, 0, &s);
        std::string textureName = s.data;
        std::cout << "Loading diffuse texture filename: " << s.data << std::endl;
        (*geometry)->texture = pgr::createTexture(textureName);
    }
    CHECK_GL_ERROR();

    (*geometry)->specularTexture = 0;

    // load texture image
    if (mat->GetTextureCount(aiTextureType_SPECULAR) > 0) {
        // get texture name
        aiString s;
        mat->GetTexture(aiTextureType_SPECULAR, 0, &s);
        std::string textureName = s.data;
        std::cout << "Loading specular texture filename: " << s.data << std::endl;
        (*geometry)->specularTexture = pgr::createTexture(textureName);
    }
    CHECK_GL_ERROR();

    glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
    glBindVertexArray((*geometry)->vertexArrayObject);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->elementBufferObject); // bind our element array buffer (indices) to vao
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);

    glEnableVertexAttribArray(shader.positionLocation);
    glVertexAttribPointer(shader.positionLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);

    if (useLighting == true) {
        std::cout << "shd.normalLoc  " << shader.normalLocation << std::endl;
        glEnableVertexAttribArray(shader.normalLocation);
        glVertexAttribPointer(shader.normalLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*) (3 * sizeof (float) * mesh->mNumVertices));
    } else {
        glDisableVertexAttribArray(shader.colorLocation);
        // following line is problematic on AMD/ATI graphic cards
        // -> if you see black screen (no objects at all) than try to set color manually in vertex shader to see at least something
        glVertexAttrib3f(shader.colorLocation, color.r, color.g, color.b);
    }
    CHECK_GL_ERROR();
    std::cout << "shd.texCorddLoc  " << shader.texCoordLocation << std::endl;
    glEnableVertexAttribArray(shader.texCoordLocation);
    glVertexAttribPointer(shader.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 0, (void*) (6 * sizeof (float) * mesh->mNumVertices));
    CHECK_GL_ERROR();

    glBindVertexArray(0);

    (*geometry)->numTriangles = mesh->mNumFaces;
    std::cout << "end loading " << fileName << std::endl;
    return true;
}