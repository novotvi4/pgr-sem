/* 
 * File:   init.h
 * Author: viky
 *
 * Created on 13. prosince 2016, 12:42
 */

#pragma once
#include <list>
#include "data.h"
#include "Camera.h"
#include "render.h"

typedef std::vector<void *> GameObjectsList;

struct SceneState {
    int windowWidth;
    int windowHeight;

    bool freeCameraMode;

    bool keyMap[KEYS_COUNT];

    float elapsedTime;

};

struct SceneObjects {
    GameObjectsList trees;
    GroundObject* ground;
    HouseObject* house;
    CartObject* cart;
    Camera* camera;
    GameObjectsList glowWorms;
    SkyBoxObject* skyBox;
    CubeObject* cube;
    ExplosionObject* explosion;
};

TreeObject* createTree(SceneState sceneState);
GroundObject * createGround(SceneState sceneState);
GlowWormObject * createGlowWorm(SceneState sceneState, glm::vec3 color);
SkyBoxObject * createSkyBox(SceneState sceneState);
CubeObject * createCubeObject(SceneState sceneState);

