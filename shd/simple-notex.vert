#version 330 core

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
in vec3 position;    
in vec3 color;
in vec3 normal;             // vertex normal

out vec4 color_v;
out vec3 normal_v;
out vec3 fragPosition;

void main() {
    mat4 pvm = projectionMatrix * viewMatrix * modelMatrix;
    gl_Position = pvm * vec4(position, 1.0);
    color_v = vec4(color, 1.0);

    normal_v = mat3(transpose(inverse(modelMatrix))) * normal;
    fragPosition = vec3(modelMatrix * vec4(position, 1.0f));
}
