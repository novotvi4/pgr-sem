#include "pgr.h"
#include "render.h"
#include "helpers.h"
#include "init.h"

TreeObject* createTree(SceneState sceneState) {
    TreeObject* newTree = new TreeObject;

    newTree->startTime = sceneState.elapsedTime;
    newTree->currentTime = newTree->startTime;

    newTree->size = 0.4f;

    newTree->direction = glm::vec3(0.0f, 0.0f, 1.0f);

    // position is generated randomly as well
    newTree->position = generateRandomPosition();
    newTree->position.y = 0.2f;

    return newTree;
}

GroundObject * createGround(SceneState sceneState) {
    GroundObject* newGround = new GroundObject;
    newGround->startTime = sceneState.elapsedTime;
    newGround->currentTime = newGround->startTime;
    newGround->size = GROUND_SIZE;
    newGround->position = glm::vec3(0.0f, 0.0f, 0.0f);
    return newGround;
}

GlowWormObject * createGlowWorm(SceneState sceneState, glm::vec3 color) {
    GlowWormObject* ngw = new GlowWormObject;
    ngw->startTime = sceneState.elapsedTime;
    ngw->currentTime = ngw->startTime;
    ngw->size = 0.0f;
    ngw->speed = GLOW_WORM_SPEED;
    ngw->position = glm::vec3(0.0f, 0.0f, 0.0f);
    ngw->color = color;
    return ngw;
}

SkyBoxObject * createSkyBox(SceneState sceneState) {
    SkyBoxObject* nsk = new SkyBoxObject;
    nsk->startTime = sceneState.elapsedTime;
    nsk->currentTime = nsk->startTime;
    nsk->size = 1.0f;
    nsk->position = glm::vec3(0.0f, 0.0f, 0.0f);
    return nsk;
}
