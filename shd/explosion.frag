#version 140

uniform float time;
uniform mat4 Vmatrix;         
uniform sampler2D texSampler; 

smooth in vec3 position_v;    
smooth in vec2 texCoord_v;    

out vec4 color_f;             


uniform ivec2 pattern = ivec2(8, 2);
uniform float frameDuration = 0.1f;

vec4 sampleTexture(int frame) {
  vec2 offset = vec2(1.0) / vec2(pattern);
  vec2 texCoordBase = texCoord_v / vec2(pattern);
  vec2 texCoord = texCoordBase + vec2(frame % pattern.x, (frame / pattern.x)) * offset;
  return texture(texSampler, texCoord);
}

void main() {
  int frame = int(time / frameDuration);
  color_f = sampleTexture(frame);
}
