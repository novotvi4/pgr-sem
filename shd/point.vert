#version 330 core

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
in vec3 position;    

void main() {
    mat4 pvm = projectionMatrix * viewMatrix * modelMatrix;
    gl_Position = pvm * vec4(position, 1.0);
}