#version 330 core

struct Material {
  sampler2D diffuse;
  sampler2D specular;           
  float     shininess;
};
uniform Material material;

struct DirLight {
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};  
uniform DirLight dirLight;
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);  

struct PointLight {    
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;  

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};  
#define NR_POINT_LIGHTS 4  
uniform PointLight pointLights[NR_POINT_LIGHTS];
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);  

struct SpotLight {
    vec3 position;
    vec3 direction;
    float cutOff;
    float outerCutOff;
  
    float constant;
    float linear;
    float quadratic;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;       
};
uniform SpotLight spotLight;
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

//fog
struct Fog{
    vec4  color;				
    float start;				
    float end;	
    float density;
    int   type;
};
uniform Fog fog;
float getFogFactor(Fog params, float fogCoord);

//lighting
uniform vec3 viewPos;
in vec3 normal_v;
in vec3 fragPosition;
in vec4 vEyeSpacePos; 

in vec2 texCoord_v;
in vec4 color_v;
flat in int  noTexture;

out vec4 outputColor;

void main() {
     // Properties
    vec3 norm = normalize(normal_v);
    vec3 viewDir = normalize(viewPos - fragPosition);
    vec3 result;

    result = CalcDirLight(dirLight, norm, viewDir);
    for(int i = 0; i < NR_POINT_LIGHTS; i++){
        result += CalcPointLight(pointLights[i], norm, fragPosition, viewDir);    
    }
    // Phase 3: Spot light
    result += CalcSpotLight(spotLight, norm, fragPosition, viewDir);    
    
    outputColor = vec4(result, 1.0);

    // Add fog
    float fogCoord = abs(vEyeSpacePos.z/vEyeSpacePos.w);
    outputColor = mix(outputColor, fog.color, getFogFactor(fog, fogCoord));
}

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir){
    vec3 lightDir = normalize(-light.direction);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    
    vec3 ambColor;
    vec3 difColor;
    vec3 speColor;
    if(noTexture == 1){
        ambColor = color_v.xyz;
        difColor = color_v.xyz;
        speColor = color_v.xyz;
    }else{
        ambColor = vec3(texture(material.diffuse, texCoord_v));
        difColor = vec3(texture(material.diffuse, texCoord_v));
        speColor = vec3(texture(material.specular, texCoord_v));
    }

    vec3 ambient  = light.ambient  * ambColor;
    vec3 diffuse  = light.diffuse  * diff * difColor;
    vec3 specular = light.specular * spec * speColor;
    return (ambient + diffuse + specular);
} 

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir){
    vec3 lightDir = normalize(light.position - fragPos);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // Attenuation
    float distance    = length(light.position - fragPos);
    float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));    
    
    vec3 ambColor;
    vec3 difColor;
    vec3 speColor;
    if(noTexture == 1){
        ambColor = color_v.xyz;
        difColor = color_v.xyz;
        speColor = color_v.xyz;
    }else{
        ambColor = vec3(texture(material.diffuse, texCoord_v));
        difColor = vec3(texture(material.diffuse, texCoord_v));
        speColor = vec3(texture(material.specular, texCoord_v));
    }

    vec3 ambient  = light.ambient  * ambColor;
    vec3 diffuse  = light.diffuse  * diff * difColor;
    vec3 specular = light.specular * spec * speColor;
    ambient  *= attenuation;
    diffuse  *= attenuation;
    specular *= attenuation;
    return (ambient + diffuse + specular);
}

vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir){
    vec3 lightDir = normalize(light.position - fragPos);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // Attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));    
    // Spotlight intensity
    float theta = dot(lightDir, normalize(-light.direction)); 
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    
    vec3 ambColor;
    vec3 difColor;
    vec3 speColor;
    if(noTexture == 1){
        ambColor = color_v.xyz;
        difColor = color_v.xyz;
        speColor = color_v.xyz;
    }else{
        ambColor = vec3(texture(material.diffuse, texCoord_v));
        difColor = vec3(texture(material.diffuse, texCoord_v));
        speColor = vec3(texture(material.specular, texCoord_v));
    }

    vec3 ambient  = light.ambient  * ambColor;
    vec3 diffuse  = light.diffuse  * diff * difColor;
    vec3 specular = light.specular * spec * speColor;

    ambient *= attenuation * intensity;
    diffuse *= attenuation * intensity;
    specular *= attenuation * intensity;
    return (ambient + diffuse + specular);
}

float getFogFactor(Fog params, float fogCoord){
   float fResult = 0.0;
   if(params.type == 0)
      fResult = (params.end-fogCoord)/(params.end-params.start);
   else if(params.type == 1)
      fResult = exp(-params.density*fogCoord);
   else if(params.type == 2)
      fResult = exp(-pow(params.density*fogCoord, 2.0));
      
   fResult = 1.0-clamp(fResult, 0.0, 1.0);
   
   return fResult;
}