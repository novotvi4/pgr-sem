#version 330 core

struct Material {           // structure that describes currently used material
  vec3  ambient;            // ambient component
  vec3  diffuse;            // diffuse component
  vec3  specular;           // specular component
  float shininess;          // sharpness of specular reflection
};

uniform Material material;

struct Light {
    vec3 position;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
uniform Light light;  

//lighting
uniform vec3 lightPosition;
uniform vec3 lightColor;
uniform vec3 viewPos;
in vec3 normal_v;
in vec3 fragPosition;

in vec4 color_v;
out vec4 outputColor;

void main() {

    vec3 ambient = light.ambient * material.ambient;
    
    vec4 texColor = color_v;

    //diffuse
    vec3 norm = normalize(normal_v);
    vec3 lightDir = normalize(lightPosition - fragPosition);  
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * (diff * material.diffuse);

    //specular
    vec3 viewDir = normalize(viewPos - fragPosition);
    vec3 reflectDir = reflect(-lightDir, norm);
    
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);   
    vec3 specular = light.specular * (spec * material.specular);  
    
    outputColor =  vec4(ambient + diffuse + specular,0.0f)* texColor;
}
