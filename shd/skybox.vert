#version 330 core
in vec3 position;
out vec3 texCoords_v;
out vec4 vEyeSpacePos;

uniform mat4 projection;
uniform mat4 view;


void main(){
    gl_Position =   projection * view * vec4(position, 1.0);  
    texCoords_v = position;

    vec4 vEyeSpacePosVertex = view * vec4(position, 1.0);
    vEyeSpacePos = vEyeSpacePosVertex;  
} 