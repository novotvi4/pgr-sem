#include <iostream>
#include "helpers.h"

GLuint loadCubemap(std::vector<const GLchar*> faces) {
    GLuint textureID;
    glGenTextures(1, &textureID);
    glActiveTexture(GL_TEXTURE0);

    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
    for (GLuint i = 0; i < faces.size(); i++) {
        //        image = SOIL_load_image(faces[i], &width, &height, 0, SOIL_LOAD_RGB);
        //        glTexImage2D(
        //                GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
        //                GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image
        //                );
        pgr::loadTexImage2D(faces[i], GL_TEXTURE_CUBE_MAP_POSITIVE_X + i);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return textureID;
}

/**
 * Checks whether vector is zero-length or not.
 */
bool isVectorNull(glm::vec3 &vect) {

    return !vect.x && !vect.y && !vect.z;
}

/**
 * @return random position with y==0.0
 */
glm::vec3 generateRandomPosition() {
    glm::vec3 newPosition;

    // position is generated randomly
    // coordinates are in range -1.0f ... 1.0f
    newPosition = glm::vec3(
            (float) (2.0 * (rand() / (double) RAND_MAX) - 1.0),
            0.0f,
            (float) (1.0 * (rand() / (double) RAND_MAX) - 1.0)
            );

    return newPosition;
}

/**
 This function works similarly to \ref gluLookAt, however it is used for object transform
 rather than view transform. The current coordinate system is moved so that origin is moved
 to the \a position. Object's local front (-Z) direction is rotated to the \a front and
 object's local up (+Y) direction is rotated so that angle between its local up direction and
 \a up vector is minimum.

 \param[in]  position           Position of the origin.
 \param[in]  front              Front direction.
 \param[in]  up                 Up vector.
 */
glm::mat4 alignObject(glm::vec3& position, const glm::vec3& front, const glm::vec3& up) {

    glm::vec3 z = -glm::normalize(front);

    if (isVectorNull(z))
        z = glm::vec3(0.0, 0.0, 1.0);

    glm::vec3 x = glm::normalize(glm::cross(up, z));

    if (isVectorNull(x))
        x = glm::vec3(1.0, 0.0, 0.0);

    glm::vec3 y = glm::cross(z, x);
    //mat4 matrix = mat4(1.0f);
    glm::mat4 matrix = glm::mat4(
            x.x, x.y, x.z, 0.0,
            y.x, y.y, y.z, 0.0,
            z.x, z.y, z.z, 0.0,
            position.x, position.y, position.z, 1.0
            );

    return matrix;
}

void printVec3(const glm::vec3 vec, const std::string msg) {
    std::cout << msg << vec.r << " " << vec.g << " " << vec.b << std::endl;
}

//**************************************************************************************************
/// Evaluates a position on Catmull-Rom curve segment.
/**
  \param[in] P0       First control point of the curve segment.
  \param[in] P1       Second control point of the curve segment.
  \param[in] P2       Third control point of the curve segment.
  \param[in] P3       Fourth control point of the curve segment.
  \param[in] t        Curve segment parameter. Must be within range [0, 1].
  \return             Position on the curve for parameter \a t.
*/
glm::vec3 evaluateCurveSegment(
    glm::vec3&  P0,
    glm::vec3&  P1,
    glm::vec3&  P2,
    glm::vec3&  P3,
    const float t
) {
  glm::vec3 result(0.0, 0.0, 0.0);

  // evaluate point on a curve segment (defined by the control points P0...P3)
  // for the given value of parametr t

  const float t2 = t*t;
  const float t3 = t*t2;

  result = P0 * (-      t3 + 2.0f*t2 - t       )
      + P1 * (  3.0f*t3 - 5.0f*t2     + 2.0f)
      + P2 * (- 3.0f*t3 + 4.0f*t2 + t       )
      + P3 * (       t3 -      t2           );

  result *= 0.5f;

  return result;
}
//**************************************************************************************************
/// Evaluates a first derivative of Catmull-Rom curve segment.

/**
  \param[in] P0       First control point of the curve segment.
  \param[in] P1       Second control point of the curve segment.
  \param[in] P2       Third control point of the curve segment.
  \param[in] P3       Fourth control point of the curve segment.
  \param[in] t        Curve segment parameter. Must be within range [0, 1].
  \return             First derivative of the curve for parameter \a t.
 */
glm::vec3 evaluateCurveSegment_1stDerivative(
        glm::vec3& P0,
        glm::vec3& P1,
        glm::vec3& P2,
        glm::vec3& P3,
        const float t
        ) {
    glm::vec3 result(1.0, 0.0, 0.0);

    // evaluate first derivative for a point on a curve segment (defined by the control points P0...P3)
    // for the given value of parametr t

    const float t2 = t*t;

    result = P0 * (-3.0f * t2 + 4.0f * t - 1.0f)
            + P1 * (9.0f * t2 - 10.0f * t)
            + P2 * (-9.0f * t2 + 8.0f * t + 1.0f)
            + P3 * (3.0f * t2 - 2.0f * t);

    result *= 0.5f;

    return result;
}

//**************************************************************************************************
/// Evaluates a position on a closed curve composed of Catmull-Rom segments.

/**
  \param[in] points   Array of curve control points.
  \param[in] count    Number of curve control points.
  \param[in] t        Parameter for which the position shall be evaluated.
  \return             Position on the curve for parameter \a t.
  \note               Although the range of the paramter is from [0, \a count] (outside the range
                      the curve is periodic) one must presume any value (even negative).
 */
glm::vec3 evaluateClosedCurve(
        glm::vec3 points[],
        const size_t count,
        const float t
        ) {
    glm::vec3 result(0.0, 0.0, 0.0);

    // based on the value of parametr t first find corresponding segment and its control points => i
    // and then call evaluateCurveSegment function with proper parameters to get a point on a closed curve

    float param = cyclic_clamp(t, 0.0f, float(count));
    size_t index = size_t(param);

    result = evaluateCurveSegment(
            points[(index - 1 + count) % count],
            points[(index) % count],
            points[(index + 1) % count],
            points[(index + 2) % count],
            param - floor(param)
            );

    return result;
}

//**************************************************************************************************
/// Evaluates a first derivative of a closed curve composed of Catmull-Rom segments.

/**
  \param[in] points   Array of curve control points.
  \param[in] count    Number of curve control points.
  \param[in] t        Parameter for which the derivative shall be evaluated.
  \return             First derivative of the curve for parameter \a t.
  \note               Although the range of the paramter is from [0, \a count] (outside the range
                      the curve is periodic) one must presume any value (even negative).
 */
glm::vec3 evaluateClosedCurve_1stDerivative(
        glm::vec3 points[],
        const size_t count,
        const float t
        ) {
    glm::vec3 result(1.0, 0.0, 0.0);

    // based on the value of parametr t first find corresponding curve segment and its control points => i
    // and then call evaluateCurveSegment_1stDerivative function with proper parameters
    // to get a derivative for the given point on a closed curve

    float param = cyclic_clamp(t, 0.0f, float(count));
    size_t index = size_t(param);

    result = evaluateCurveSegment_1stDerivative(
            points[(index - 1 + count) % count],
            points[(index) % count],
            points[(index + 1) % count],
            points[(index + 2) % count],
            param - floor(param)
            );

    return result;
}

//**************************************************************************************************
/// Cyclic clamping of a value.

/**
 Makes sure that value is not outside the internal [\a minBound, \a maxBound].
 If \a value is outside the interval it treated as periodic value with period equal to the size
 of the interval. A necessary number of periods are added/subtracted to fit the value to the interval.

 \param[in]  value              Value to be clamped.
 \param[in]  minBound           Minimum bound of value.
 \param[in]  maxBound           Maximum bound of value.
 \return                        Value within range [minBound, maxBound].
 \pre                           \a minBound is not greater that \maxBound.
 */
template <typename T>T cyclic_clamp(const T value, const T minBound, const T maxBound) {

    T amp = maxBound - minBound;
    T val = fmod(value - minBound, amp);

    if (val < T(0))
        val += amp;

    return val + minBound;
}