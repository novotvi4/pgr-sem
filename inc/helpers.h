
#pragma once

#include "pgr.h" // glm

GLuint loadCubemap(std::vector<const GLchar*> faces);

//**************************************************************************************************
/// Checks whether vector is zero-length or not.
bool isVectorNull(glm::vec3 &vect);

glm::vec3 generateRandomPosition();
//**************************************************************************************************
/// Align (rotate and move) current coordinate system to given parameters.
/**
 This function work similarly to \ref gluLookAt, however it is used for object transform
 rather than view transform. The current coordinate system is moved so that origin is moved
 to the \a position. Object's local front (-Z) direction is rotated to the \a front and
 object's local up (+Y) direction is rotated so that angle between its local up direction and
 \a up vector is minimum.

 \param[in]  position           Position of the origin.
 \param[in]  front              Front direction.
 \param[in]  up                 Up vector.
 */
glm::mat4 alignObject(glm::vec3& position, const glm::vec3& front, const glm::vec3& up);

void printVec3(const glm::vec3 vec, const std::string msg);

glm::vec3 evaluateCurveSegment(
    glm::vec3&  P0,
    glm::vec3&  P1,
    glm::vec3&  P2,
    glm::vec3&  P3,
    const float t
);
glm::vec3 evaluateCurveSegment_1stDerivative(
        glm::vec3& P0,
        glm::vec3& P1,
        glm::vec3& P2,
        glm::vec3& P3,
        const float t
        );

glm::vec3 evaluateClosedCurve(
        glm::vec3 points[],
        const size_t count,
        const float t
        );

glm::vec3 evaluateClosedCurve_1stDerivative(
        glm::vec3 points[],
        const size_t count,
        const float t
        );

template <typename T>T cyclic_clamp(const T value, const T minBound, const T maxBound);