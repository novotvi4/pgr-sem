
/* 
 * File:   Camera.h
 * Author: viky
 *
 * Created on 13. prosince 2016, 13:16
 */

#pragma once

#include <vector>
#include <iostream>
#include "pgr.h"

enum Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

// Default camera values
const GLfloat YAW = -90.0f;
const GLfloat PITCH = 0.0f;
const GLfloat SPEED = 0.3f;
const GLfloat SENSITIVTY = 0.25f;

class Camera {
public:

    Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = YAW, GLfloat pitch = PITCH);

    /**
     * @return projection matrix 
     */
    glm::mat4 getViewMatrix();

    /**
     * moving aroud scene by keyboard
     * @param direction by Camera_Movement enum to abstract from specific keyboard
     * @param deltaTime
     */
    void processKeyboard(Camera_Movement direction, GLfloat deltaTime);

    /**
     * looking around by mouse
     * @param xoffset
     * @param yoffset
     * @param constrainPitch to prevent world desctruction by fliping 
     */
    void processMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true);

    void reset();

    void setView1();

    void setView2();

    void setFlying(bool set);

    bool isFlying();

    GLfloat getMouseSensitivity() const;

    void setMouseSensitivity(GLfloat mouseSensitivity);

    GLfloat getMovementSpeed() const;

    void setMovementSpeed(GLfloat movementSpeed);

    glm::vec3 getPosition() const;
    
    glm::vec3 getFront() const;

    void setPosition(glm::vec3 position);
    
    void setFront(glm::vec3 front);

private:

    void updateCameraVectors();

    glm::vec3 position; 
    glm::vec3 front;
    glm::vec3 up;
    glm::vec3 right;
    glm::vec3 worldUp; 

    GLfloat yaw;   //looking around
    GLfloat pitch; //looking up and down

    GLfloat movementSpeed;
    GLfloat mouseSensitivity;

    bool flyingEnabled;
};
