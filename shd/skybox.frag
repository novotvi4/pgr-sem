#version 330 core
in vec3 texCoords_v;
in vec4 vEyeSpacePos; 
out vec4 color;

//fog
struct Fog{
    vec4  color;				
    float start;				
    float end;	
    float density;
    int   type;
};
uniform Fog fog;
float getFogFactor(Fog params, float fFogCoord);

uniform samplerCube skybox;

void main(){
    color = texture(skybox, texCoords_v);
    // Add fog
    float fFogCoord = abs(vEyeSpacePos.z/vEyeSpacePos.w);
    color = mix(color, fog.color, getFogFactor(fog, fFogCoord));
}

float getFogFactor(Fog params, float fFogCoord){
   float fResult = 0.0;
   if(params.type == 0)
      fResult = (params.end-fFogCoord)/(params.end-params.start);
   else if(params.type == 1)
      fResult = exp(-params.density*fFogCoord);
   else if(params.type == 2)
      fResult = exp(-pow(params.density*fFogCoord, 2.0));
      
   fResult = 1.0-clamp(fResult, 0.0, 1.0);
   
   return fResult;
}